# tickers

<!--
[![PyPI - Version](https://img.shields.io/pypi/v/tickers.svg)](https://pypi.org/project/tickers)
[![PyPI - Python Version](https://img.shields.io/pypi/pyversions/tickers.svg)](https://pypi.org/project/tickers)
-->

-----

Tickers is a script for generating spreadsheets with data harvested from Yahoo Finance's API.

**Table of Contents**

- [Installation](#installation)
- [License](#license)

## Installation

I recommend using pipx to install into an isolated environment.

```console
git clone https://gitlab.com/effigies/tickers.git
pipx install ./tickers
```

## Features

* Retrieve one or more tickers
* Date ranges are flexibly specifiable
* Forward or backward filling for missing data
* Values rounded to nearest 100th of a cent
* Output to ODS, XLSX or displayed in terminal

## Credit

This script was inspired by http://finance.jasonstrimpel.com/bulk-stock-download/

## License

`tickers` is distributed under the terms of the [MIT](https://spdx.org/licenses/MIT.html) license.
