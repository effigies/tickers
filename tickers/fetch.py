# SPDX-FileCopyrightText: 2023-present Chris Markiewicz <effigies@gmail.com>
#
# SPDX-License-Identifier: MIT
import io
import logging
from datetime import date, datetime, time
from functools import lru_cache

import pandas as pd
import requests

ENDPOINT = 'https://api.tiingo.com/tiingo/daily/{symbol}/prices'
SESSION = requests.Session()
SESSION.headers.update(
    {
        # 'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) '
        # 'AppleWebKit/537.36 (KHTML, like Gecko) '
        # 'Chrome/91.0.4472.124 Safari/537.36',
        'Content-Type': 'application/json',
        'Authorization': 'Token e59ff21426391e9026629001a137a24508c86c5e',
    }
)


def get_historical_data(
    symbol: str,
    start_date: date,
    end_date: date,
) -> pd.DataFrame:
    params = {
        'startDate': f'{start_date:%Y-%m-%d}',
        'endDate': f'{end_date:%Y-%m-%d}',
        'sort': '-date',
        'format': 'csv',
    }
    logging.getLogger('tickers').info(f'Retrieving {symbol}')
    res = SESSION.get(ENDPOINT.format(symbol=symbol), params=params)
    if res.ok:
        return pd.read_csv(
            io.BytesIO(res.content), index_col='date', parse_dates=True
        )
    else:
        logging.getLogger('tickers').debug(f'Request: {res.request}')
        logging.getLogger('tickers').debug(f'Response: {res}')
    raise ValueError(f'Could not retrieve symbol {symbol}')


@lru_cache()  # Cache columns to permit duplicates without a second API call
def get_observations(
    symbol: str,
    field: str,
    start_date: date,
    end_date: date,
) -> pd.DataFrame:
    orig = get_historical_data(symbol, start_date, end_date)[field]
    return pd.DataFrame({symbol: orig}, index=orig.index)


def get_tickers(
    symbols: list[str],
    field: str,
    start_date: date,
    end_date: date,
) -> pd.DataFrame:
    return pd.concat(
        (
            get_observations(symbol, field, start_date, end_date)
            for symbol in symbols
        ),
        axis=1,
    )
