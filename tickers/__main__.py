import argparse
import logging
import typing as ty
from datetime import datetime, timedelta
from functools import partial
from pathlib import Path

import pandas as pd
from dateutil.parser import parse as parsedate
from .fetch import get_tickers


if ty.TYPE_CHECKING:
    DFFilter = ty.Callable[[pd.DataFrame], pd.DataFrame] | partial[pd.DataFrame]


def main() -> None:
    fields = 'open,high,low,close,volume'.split(',')
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-s', '--symbols', required=True, help='Comma-delimited list or file'
    )
    parser.add_argument(
        '-f',
        '--field',
        default='close',
        metavar='FIELD',
        choices=fields,
        help=f'One of {fields}',
    )
    parser.add_argument('-o', '--output', metavar='PATH')
    parser.add_argument('-n', '--fillna', choices=('forward', 'none'))
    parser.add_argument('-v', '--verbose', action='count', default=0)
    parser.add_argument(
        'start_date', nargs='?', help='First date (default 1 week ago)'
    )
    parser.add_argument(
        'end_date', nargs='?', help='Final date (default today)'
    )
    opts = parser.parse_args()

    logging.basicConfig(level=('WARNING', 'INFO', 'DEBUG')[opts.verbose])

    end = parsedate(opts.end_date) if opts.end_date else datetime.today()
    start = (
        parsedate(opts.start_date)
        if opts.start_date
        else end - timedelta(weeks=1)
    )

    if end < start:
        raise ValueError(
            f'Start date ({start}) cannot come after end date ({end})'
        )

    sym_path = Path(opts.symbols)
    if sym_path.exists():
        symbols = [sym for sym in sym_path.read_text().splitlines() if sym]
    else:
        symbols = opts.symbols.split(',')

    filterna: DFFilter = pd.DataFrame.dropna
    if opts.fillna:
        kwargs: dict[str, int | str] = {}
        if opts.fillna == 'forward':
            kwargs['method'] = 'ffill'
        elif opts.fillna == 'none':
            kwargs['value'] = 0
        filterna = partial(pd.DataFrame.fillna, **kwargs)

    df = get_tickers(symbols, opts.field, start, end)
    df = filterna(df).round(4).sort_index(ascending=False)

    if opts.output:
        df.to_excel(opts.output)
    else:
        print(df)


if __name__ == '__main__':
    main()
